package frederick.mytaxi.api

import frederick.mytaxi.vo.Response
import frederick.mytaxi.vo.Vehicle
import retrofit2.http.GET
import retrofit2.http.Query
import rx.Observable

interface TaxiService {

    @GET("/")
    fun getVehicles(@Query("p1Lat") latitude1: Double, @Query("p1Lon") longitude1: Double,
                    @Query("p2Lat") latitude2: Double, @Query("p2Lon") longitude2: Double): Observable<Response<List<Vehicle>>>

    companion object {
        public val BASE_URL: String = "https://fake-poi-api.mytaxi.com/"
        public val HAMBURG_P1_LAT = 53.694865
        public val HAMBURG_P1_LON = 9.757589

        public val HAMBURG_P2_LAT = 53.394655
        public val HAMBURG_P2_LON = 10.099891

        public val VEHICLES_ENDPOINT = ""
    }
}