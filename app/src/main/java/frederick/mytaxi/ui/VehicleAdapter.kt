package frederick.mytaxi.ui

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import frederick.mytaxi.databinding.ListItemVehicleBinding
import frederick.mytaxi.vo.Vehicle

open class VehicleAdapter : RecyclerView.Adapter<VehicleAdapter.ViewHolder>() {

    var vehicles: ArrayList<Vehicle>? = null
        set(value) {
            field = value
            this.notifyDataSetChanged()
        }

    inner class ViewHolder(internal var binding: ListItemVehicleBinding) : RecyclerView.ViewHolder(binding.root), View.OnClickListener {

        override fun onClick(v: View?) {

            val intent = Intent(binding.root.context, MapsActivity::class.java)

            intent.putExtra("selected_vehicle_index", vehicles!!.indexOf(binding.vehicle))

            intent.putExtra("vehicles", vehicles)

            binding.root.context.startActivity(intent)
        }

        fun bind(item: Vehicle) {
            binding.vehicle = item
            binding.executePendingBindings()

            binding.root.setOnClickListener(this)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemBinding = ListItemVehicleBinding.inflate(layoutInflater, parent, false)
        return ViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(vehicles!![position])
    }

    override fun getItemId(i: Int): Long {
        return i.toLong()
    }

    override fun getItemCount(): Int {
        return if (vehicles != null) vehicles!!.size else 0
    }

    companion object {

        private val TAG = VehicleAdapter::class.java.simpleName
    }


}