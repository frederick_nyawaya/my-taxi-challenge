package frederick.mytaxi.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import frederick.mytaxi.R
import frederick.mytaxi.vo.Vehicle


class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)

        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap) {

        val selectedVehicleIndex = intent.extras.getInt("selected_vehicle_index")

        val vehicles = intent.extras.get("vehicles") as ArrayList<Vehicle>
        val selectedVehicle = vehicles[selectedVehicleIndex]

        mMap = googleMap
        vehicles.forEach {
            //Add each of the vehicles to the map
            val vehiclePosition = LatLng(it.coordinate.latitude, it.coordinate.longitude)

            val marker = MarkerOptions()
                    .position(vehiclePosition)
                    .title(it.id.toString())
                    .icon(BitmapDescriptorFactory.fromResource(if (it == selectedVehicle) R.drawable.car_selected else R.drawable.car))//use a different color for selected vehicle
                    .rotation(it.heading)

            mMap.addMarker(marker)

            //Center and Zoom in to the selected vehicle
            if (it == selectedVehicle) {
                val cameraPosition = CameraPosition.Builder()
                        .target(vehiclePosition)
                        .zoom(12f)
                        .build()
                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
            }
        }


    }
}
