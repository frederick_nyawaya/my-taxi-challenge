package frederick.mytaxi.ui

import android.app.Activity
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import frederick.mytaxi.R
import frederick.mytaxi.api.TaxiService
import frederick.mytaxi.databinding.ActivityHomeBinding
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import kotlin.concurrent.thread

class HomeActivity : Activity() {

    private lateinit var binding: ActivityHomeBinding

    private val adapter: VehicleAdapter = VehicleAdapter()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_home)

        binding.listVehicles.layoutManager = LinearLayoutManager(this)
        binding.listVehicles.adapter = adapter

        binding.listVehicles.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))

        var response = getNetworkService().getVehicles(
                TaxiService.HAMBURG_P1_LAT, TaxiService.HAMBURG_P1_LON,
                TaxiService.HAMBURG_P2_LAT, TaxiService.HAMBURG_P2_LON)

        binding.progressBar.visibility = View.VISIBLE

        thread(start = true) {
            var results = response.toBlocking().single().poiList!!

            runOnUiThread {
                adapter.vehicles = ArrayList(results)
                binding.progressBar.visibility = View.GONE
            }
        }
    }

    private fun getNetworkService(): TaxiService {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY

        val client = OkHttpClient.Builder().addInterceptor(interceptor).build()

        return Retrofit.Builder()
                .baseUrl(TaxiService.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(client)
                .build()
                .create<TaxiService>(TaxiService::class.java!!)
    }

    companion object {
        val TAG = HomeActivity::class.java.simpleName
    }
}
