package frederick.mytaxi.vo

import java.io.Serializable

data class Vehicle(val id: Int, val coordinate: Coordinate, val fleetType: String, val heading: Float): Serializable

data class Coordinate(val latitude: Double, val longitude: Double): Serializable

class Response<T> {
    var poiList: T? = null
}