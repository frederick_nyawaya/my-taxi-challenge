package frederick.mytaxi.ui


import android.view.View
import android.view.ViewGroup
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import frederick.mytaxi.R
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers.`is`
import org.hamcrest.Matchers.allOf
import org.hamcrest.TypeSafeMatcher
import org.hamcrest.core.IsInstanceOf
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@LargeTest
@RunWith(AndroidJUnit4::class)
class HomeActivityTest {

    @Rule
    @JvmField
    var mActivityTestRule = ActivityTestRule(HomeActivity::class.java)

    @Test
    fun homeActivityTest() {
        val viewGroup = onView(
                allOf(childAtPosition(
                        allOf(withId(R.id.list_vehicles),
                                childAtPosition(
                                        IsInstanceOf.instanceOf(android.widget.FrameLayout::class.java),
                                        0)),
                        0),
                        isDisplayed()))
        viewGroup.check(matches(isDisplayed()))

        val viewGroup2 = onView(
                allOf(childAtPosition(
                        allOf(withId(R.id.list_vehicles),
                                childAtPosition(
                                        IsInstanceOf.instanceOf(android.widget.FrameLayout::class.java),
                                        0)),
                        0),
                        isDisplayed()))
        viewGroup2.check(matches(isDisplayed()))

        val constraintLayout = onView(
                allOf(childAtPosition(
                        allOf(withId(R.id.list_vehicles),
                                childAtPosition(
                                        withClassName(`is`("android.widget.FrameLayout")),
                                        1)),
                        4),
                        isDisplayed()))
        constraintLayout.perform(click())

        val constraintLayout2 = onView(
                allOf(childAtPosition(
                        allOf(withId(R.id.list_vehicles),
                                childAtPosition(
                                        withClassName(`is`("android.widget.FrameLayout")),
                                        1)),
                        4),
                        isDisplayed()))
        constraintLayout2.perform(click())

        val frameLayout = onView(
                allOf(IsInstanceOf.instanceOf(android.widget.FrameLayout::class.java), isDisplayed()))
        frameLayout.check(matches(isDisplayed()))
    }

    private fun childAtPosition(
            parentMatcher: Matcher<View>, position: Int): Matcher<View> {

        return object : TypeSafeMatcher<View>() {
            override fun describeTo(description: Description) {
                description.appendText("Child at position $position in parent ")
                parentMatcher.describeTo(description)
            }

            public override fun matchesSafely(view: View): Boolean {
                val parent = view.parent
                return parent is ViewGroup && parentMatcher.matches(parent)
                        && view == parent.getChildAt(position)
            }
        }
    }
}
